package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;


public class LoginPO extends  BasePO{

    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc='username']")
    MobileElement txtUserName;

    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc='password']")
    MobileElement txtPassword;

    @AndroidFindBy(accessibility = "login")
    MobileElement btnLogin;



    public LoginPO(AppiumDriver driver){
        super(driver);
    }

    public void enterCredentials(){
        sendKeysMobile(txtUserName,"admin");
        sendKeysMobile(txtPassword,"admin");
        clickElement(btnLogin);

    }

}
